/*
 
BufferedWriter is more efficient if you

have multiple writes between flush/close
the writes are small compared with the buffer size.
*/



import java.io.*;


public class ExJava_WriteTxt2File {

 
	public static void main(String[] args) {
	 
		System.out.println("Hello World");
		Writer writer = null;

		try {
		    writer = new BufferedWriter(new OutputStreamWriter(
		          new FileOutputStream("filename.txt"), "utf-8"));
		    writer.write("Something");
		} catch (IOException ex) {
		  // report
		} finally {
		   try {writer.close();} catch (Exception ex) {/*ignore*/}
		}
	}

}

 